FROM openjdk:8-jdk
ADD target/docker-spring-boot.jar docker-spring-boot.jar
EXPOSE 8060
ENTRYPOINT ["java","-jar","docker-spring-boot.jar"]