package com.example.SpringBootGitLabTest.service;

public interface MathService {
     double add(double a, double b);

     double remove(double a, double b);

     double multiply(double a, double b);

     double division(double a, double b);
}
